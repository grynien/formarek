var Product = require('../product');
var passport = require('passport');

var seedProduct = function() {

  Product.find({}, function(err, documents) {

    if(documents.length === 0){

      var prodArry = [
        {productName: '30 diamonds', productAmount: 20},
        {productName: '20 diamonds', productAmount: 15},
        {productName: '10 diamonds', productAmount: 10}
      ];

      for (var i = 0; i < prodArry.length; i++) {
         var data = new Product(
          {
            name: prodArry[i].productName,
            amount: prodArry[i].productAmount,
            currency: 'USD',
            forSale: true
          }
        );
        data.save();
      }

      console.log('Dummy products added!');
    }

  });

};

module.exports = seedProduct;