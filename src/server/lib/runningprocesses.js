
var python = require('./python');

var rp = {};

function startsearching(username, lat, lng) {

    if (rp[username] && rp[username].isrunning) {
        return;
    }

    if (!rp[username]) {
        rp[username] = {};
        rp[username].results = [];
    }

    rp[username].isrunning = true;


    runP(username, lat, lng);
}


function runP(username, lat, lng) {



    var pyshell = python.startScript(lat, lng);

    pyshell.on('message', function (message) {

        try {
            var mm = JSON.parse(message);

            rp[username].results.push(mm);
        } catch (e) {
            return false;
        }


    });


    pyshell.end(function (err) {
        if (err) throw err;
        rp[username].isrunning = false;
    });

}

module.exports = {
    startsearching: startsearching,
    runningProcesses: rp
};