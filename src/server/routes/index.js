var express = require('express');
var helpers = require('../lib/helpers');
var router = express.Router();


router.get('/map', helpers.ensureAuthenticated, function(req, res, next){
  res.render('map', {
    user: req.user
  });
});

router.get('/', function(req, res, next) {
  res.render('index', {
    user: req.user,
    message: req.flash('message')[0]
  });
});

router.get('/ping', function(req, res, next) {
  res.send("pong!");
});



module.exports = router;
