var express = require('express');
var router = express.Router();
var helpers = require('../lib/helpers');
var rp = require('../lib/runningprocesses');


router.get('/pokemons', helpers.ensureAuthenticated, function(req, res, next){
    var userdata = rp.runningProcesses[req.user.email];

    userdata = userdata || {};

    res.status(200).json({
      data: userdata
    });
});

router.post('/startsearching', helpers.ensureAuthenticated, function (req, res, next) {
    rp.startsearching(req.user.email, req.body.latitude, req.body.longitude);

    return res.status(200).json({
      data: '',
    });
});







module.exports = router;
