var express = require('express');
var router = express.Router();
var paypal = require('paypal-rest-sdk');

var uuid = require('node-uuid');


var helpers = require('../lib/helpers');
var User = require('../models/user.js');
var Product = require('../models/product.js');

var config = require('../../_config');

paypal.configure(config.paypal);

router.post('/order', helpers.ensureAuthenticated, function (req, res, next) {

    var userID = req.user._id;

    res.locals.session = req.session;
    var order_id = uuid.v4();

    Product.findById(req.body.productID, function (err, data) {
        if (err) {
            return next(err);
        } else {
            if (parseInt(req.body.productAmount) !== data.amount) {
                req.flash('message', {
                    status: 'danger',
                    value: 'Error!'
                });
                return res.redirect('/');
            } else {

                // Create Charge
                var paypalPayment = {
                    "intent": "sale",
                    "payer": {
                        "payment_method": "paypal"
                    },
                    "redirect_urls": {},
                    "transactions": [{
                        "amount": {
                            "currency": "USD"
                        }
                    }]
                };

                console.log(config.paypal);

                paypalPayment.transactions[0].amount.total = req.body.productAmount * 1;
                paypalPayment.redirect_urls.return_url = config.paypal.return_url + "?order_id=" + order_id + "&user_id=" + userID;
                paypalPayment.redirect_urls.cancel_url = config.paypal.cancel_url + "?status=cancel&order_id=" + order_id;
                paypalPayment.transactions[0].description = "Pokemond diamonds";


                paypal.payment.create(paypalPayment, {}, function (err, resp) {
                    if (err) {
                        req.flash('message', {
                            status: 'danger',
                            value: 'Payment API call failed'
                        });

                        return res.redirect('/');
                    }

                    if (resp) {

                        var now = (new Date()).toISOString().replace(/\.[\d]{3}Z$/, 'Z ');

                        var paypalResp = {
                            orderId: order_id,
                            respId: resp.id,
                            respState: resp.state,
                            time: now
                        }

                        // Get product details
                        User.findById(userID, function (err, data) {
                            if (err) {
                                console.log(err);

                                req.flash('message', {
                                    status: 'danger',
                                    value: 'Could not save order details'
                                });
                                return next(err);
                            } else {


                                data.products.push({ productID: req.body.productID, orderid: order_id, paypalResp: paypalResp });
                                data.save();

                                req.flash('message', {
                                    status: 'success',
                                    value: 'Thanks for purchasing a ' + req.body.productName + '!'
                                });

                                var link = resp.links;
                                for (var i = 0; i < link.length; i++) {
                                    if (link[i].rel === 'approval_url') {
                                        res.redirect(link[i].href);
                                    }
                                }


                            }
                        });
                    }
                });

            }
        }
    });
});

router.get('/orderexecute', helpers.ensureAuthenticated, function (req, res, next) {
    res.locals.session = req.session;
    User.findById(req.query.user_id, function (err, user) {
        if (err) {
            console.log(err);

            req.flash('message', {
                status: 'danger',
                value: 'Could not get the user'
            });
            return next(err);
        } else {

            var order = null;
            user.products.forEach(function (product) {
                if (product.orderid == req.query.order_id) {
                    order = product;
                }
            }, this);

            if (order.success) {
                req.flash('message', {
                    status: 'Danger',
                    value: 'Already paid'
                });

                return res.redirect('/');
            }

            var payer = { payer_id: req.query.PayerID };
            paypal.payment.execute(order.paypalResp.respId, payer, {}, function (err, resp) {
                if (err) {
                    console.log(err);
                    req.flash('message', {
                        status: 'Danger',
                        value: 'execute payment API failed'
                    });

                    res.redirect('/');
                }
                if (resp) {

                    order.success = true;
                    order.succesRes = resp;

                    user.save();


                    req.flash('message', {
                        status: 'success',
                        value: 'Payments succesful'
                    });

                    res.redirect('/auth/profile');

                }
            });
        }
    });
});

module.exports = router;