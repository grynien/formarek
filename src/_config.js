var config = {};

config.port = 8080;
config.address = "http://localhost:8080";

// mongo uri
config.mongoURI = {
  development: "mongodb://localhost/node-pokemon",
  test: "mongodb://localhost/node-pokemon-test",
  stage: process.env.MONGOLAB_URI
};

config.googleAuth = {
        clientID      : '879904626327-lk0u7vsvia8eokf1htchoo8g0sheo7f4.apps.googleusercontent.com',
        clientSecret  : '1dsxl325kqfMes68YR-goATL',
        callbackURL   : config.address + '/auth/google/callback'
    };

config.paypal = {
		host : "api.sandbox.paypal.com",
		port : "",
		client_id : "AfKoV-F8F_Qgehq9fR-VxBPRMzCAaYtOppJc0n6sK0pw12TL9W0NJ5cTMYijKx8zqfPQff6L2JUdSr7V",
		client_secret : "EMdnkD8YwmZpc1orZDxrtiiAfkhIZmCFVaevgOKQzXb0QSRreTbd6x38FBEV_MT1jraV2ZMhLF9kdJtS",
    return_url: config.address + "/paypal/orderexecute",
    cancel_url: config.address + "/paypal/cancel",
	}

module.exports = config;